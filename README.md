# Microcontroller-HMI replace for PLC

The Festo PCS system, currently available at Cao Thang Technical College, is used for teaching the practical course of Electrical and Electronic Engineering 2, utilizing control systems through EASY PORT or PLC S7-1200 and HMI. However, it poses challenges in terms of maintenance, repair, and replacement due to its high cost. Therefore, this project employs the STM32 microcontroller as a cost-effective alternative, aiming to proactively enhance maintenance and repair, as well as improve the quality of microcontroller utilization and embedded programming skills among students in the field of Electrical and Electronic Engineering.


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)

## Installation



## Usage



## Contributing



